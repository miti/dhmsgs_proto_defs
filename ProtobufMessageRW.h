//
// Created by miti on 2019-12-24.
//

#ifndef VERIFIER_PROTOBUFMESSAGERW_H
#define VERIFIER_PROTOBUFMESSAGERW_H
//using namespace google::protobuf;
#include <google/protobuf/message_lite.h>

class ProtobufMessageRW {
    int fd;
public:
    int read_msg(google::protobuf::MessageLite& message);
    int write_msg(google::protobuf::MessageLite& message);
    void set_fd(int fd);
};


#endif //VERIFIER_PROTOBUFMESSAGERW_H
